/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.AgamaController;
import Model.Agama;
import javax.swing.JOptionPane;
/**
 *
 * @author ASUS
 */
public class MasterAgama extends javax.swing.JFrame {
    Agama agama = new Agama();
    AgamaController ac = new AgamaController();

    /**
     * Creates new form MasterAgama
     */
    public MasterAgama() {
        initComponents();
        tbAgama.setModel(ac.viewAgama());
        btnhapus.setEnabled(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtnama = new javax.swing.JTextField();
        btnsimpan = new javax.swing.JButton();
        btnhapus = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbAgama = new javax.swing.JTable();
        txtid = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Agama");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 50, 80, 30));

        jLabel2.setText("Nama:");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 110, 60, 30));

        txtnama.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnamaActionPerformed(evt);
            }
        });
        getContentPane().add(txtnama, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 110, 190, 30));

        btnsimpan.setText("Simpan");
        btnsimpan.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnsimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsimpanActionPerformed(evt);
            }
        });
        getContentPane().add(btnsimpan, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 170, 80, 30));

        btnhapus.setText("Hapus");
        btnhapus.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnhapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnhapusActionPerformed(evt);
            }
        });
        getContentPane().add(btnhapus, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 170, 90, 30));

        tbAgama.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbAgama.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbAgamaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbAgama);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 260, 540, 120));

        txtid.setText("jLabel3");
        getContentPane().add(txtid, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, 50, 20));

        jMenu1.setText("Data Master");

        jMenuItem1.setText("Agama");
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Prodi");
        jMenu1.add(jMenuItem2);

        jMenuItem3.setText("Mahasiswa");
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Logout");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        setSize(new java.awt.Dimension(614, 448));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txtnamaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnamaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnamaActionPerformed

    private void btnsimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsimpanActionPerformed
        // TODO add your handling code here:
        String nama = txtnama.getText();
        if(btnsimpan.getText().equals("Simpan")){
            if(nama.isEmpty()){
                JOptionPane.showMessageDialog(null,"Harap isi nama Agama");
                txtnama.requestFocus();
                txtid.setText("");
                btnhapus.setEnabled(false);
                txtnama.setText("");
            }else{
                ac.tambahAgama(nama);
                btnhapus.setEnabled(false);
                txtid.setText("");
                txtnama.setText("");
                btnsimpan.setText("Simpan");
                txtnama.requestFocus();
                tbAgama.setModel(ac.viewAgama());
            
            }
        }else{
            if(nama.isEmpty()){
                JOptionPane.showMessageDialog(null,"Harap isi nama Agama");
                txtid.setText("");
                btnsimpan.setText("Simpan");
                btnhapus.setEnabled(false);
                txtnama.requestFocus();            
            }else{
                int id = Integer.parseInt(txtid.getText());
                ac.updateAgama(id, nama);
                txtid.setText("");
                txtnama.setText("");
                btnsimpan.setText("Simpan");
                btnhapus.setEnabled(false);
                txtnama.requestFocus();
                tbAgama.setModel(ac.viewAgama());
            }
        }
    }//GEN-LAST:event_btnsimpanActionPerformed

    private void tbAgamaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbAgamaMouseClicked
        // TODO add your handling code here:
        btnsimpan.setText("update");
        int row = tbAgama.getSelectedRow();
        txtid.setText(tbAgama.getValueAt(row, 0).toString());
        txtnama.setText(tbAgama.getValueAt(row, 1).toString());
        btnhapus.setEnabled(true);
    }//GEN-LAST:event_tbAgamaMouseClicked

    private void btnhapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnhapusActionPerformed
        // TODO add your handling code here:
        int id = Integer.parseInt(txtid.getText());
        int pilihan =JOptionPane.showConfirmDialog(null, "Apakah anda"
                + "yakin ingin menghapus data", 
                "Konfirmasi",JOptionPane.YES_NO_CANCEL_OPTION);
        if(pilihan == 0){
            ac.deleteAgama(id);
            btnhapus.setEnabled(false);
            txtnama.setText("");
            txtid.setText("");
            tbAgama.setModel(ac.viewAgama());
        }
    }//GEN-LAST:event_btnhapusActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MasterAgama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MasterAgama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MasterAgama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MasterAgama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MasterAgama().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnhapus;
    private javax.swing.JButton btnsimpan;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbAgama;
    private javax.swing.JLabel txtid;
    private javax.swing.JTextField txtnama;
    // End of variables declaration//GEN-END:variables
}
